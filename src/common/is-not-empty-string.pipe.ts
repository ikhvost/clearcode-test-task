import {ArgumentMetadata, BadRequestException, PipeTransform} from '@nestjs/common';
import {isNotEmpty, isString} from 'class-validator';

export class IsNotEmptyStringPipe implements PipeTransform {
  transform(value: string, metadata: ArgumentMetadata): string {
    if (!isString(value) || !isNotEmpty(value.trim())) throw new BadRequestException(`'${metadata.data}' should be an empty string`);
    return value;
  }
}
