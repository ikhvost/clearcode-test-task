import {ArgumentMetadata, BadRequestException, PipeTransform} from '@nestjs/common';
import {isEAN} from 'class-validator';

export class IsEANPipe implements PipeTransform {
  transform(value: string, metadata: ArgumentMetadata): string {
    if (!isEAN(value)) throw new BadRequestException(`'${metadata.data}' should be an EAN format`);
    return value;
  }
}
