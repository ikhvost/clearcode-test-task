import {BaseExceptionFilter} from '@nestjs/core';
import {ArgumentsHost, Catch, HttpException, HttpServer, Logger} from '@nestjs/common';

@Catch()
export class ExceptionFilter extends BaseExceptionFilter {

  private log = new Logger('Exception Filter');

  constructor(protected readonly applicationRef?: HttpServer) {
    super(applicationRef);
  }

  catch(exception: any, host: ArgumentsHost) {
    if(exception instanceof HttpException) {
      const req = host.getArgByIndex(1).req;
      this.log.error(`${req.method}: "${req.originalUrl}". ${exception.toString()}`);
    }
    super.catch(exception, host);
  }
}