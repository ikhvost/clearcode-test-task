import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {ApiProperty} from '@nestjs/swagger';
import {Document} from 'mongoose';
import {Constants} from '../common/constants';

@Schema({collection: Constants.ProductRef})
export class Product extends Document {
  @ApiProperty()
  @Prop({required: true})
  name: string;

  @ApiProperty()
  @Prop({required: true, unique: true})
  barcode: string;

  @ApiProperty()
  @Prop()
  allergens: string[];
}

export const ProductSchema = SchemaFactory.createForClass(Product);
