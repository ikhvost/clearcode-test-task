import {BadRequestException, HttpService, Injectable, NotFoundException} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';
import {UserService} from '../user/user.service';
import {Product} from './product';
import {ProductRepository} from './product.repository';

@Injectable()
export class ProductService {
  private readonly getProductEndpoint = 'api/v0/product';

  constructor(private readonly http: HttpService,
              private readonly config: ConfigService,
              private readonly userService: UserService,
              private readonly productRepository: ProductRepository) {
  }

  public async add(barcode: string): Promise<Product> {
    await this.checkIfProductAlreadyExists(barcode);
    const product = await this.findByBarcode(barcode);
    return this.productRepository.create(product);
  }

  public async getByUser(username: string): Promise<Product[]> {
    const user = await this.userService.getOneByName(username);
    return this.productRepository.findByUser({allergens: {$nin: user.allergens}});
  }

  private async checkIfProductAlreadyExists(barcode: string): Promise<void> {
    const product = await this.productRepository.findOne({barcode});
    if(product) throw new BadRequestException(`Product with name '${product.name}' and barcode '${product.barcode}' already exists`);
  }

  private async findByBarcode(barcode: string): Promise<Product> {
    const result = await this.http.get(`${this.config.get('OPEN_FOOD_FACTS_API')}/${this.getProductEndpoint}/${barcode}`).toPromise();
    if(!result?.data?.status) throw new NotFoundException('Product not found');

    const data = result?.data?.product;
    const allergens =
      data?.allergens_hierarchy
        .filter(allergen => allergen.split(':')[0] === 'en')
        .map(allergen => allergen.split(':')[1]);

    const product = {barcode, allergens, name: data?.product_name};

    return product as Product;
  }
}
