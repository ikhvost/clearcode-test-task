import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model, MongooseFilterQuery} from 'mongoose';
import {Constants} from '../common/constants';
import {Product} from './product';

@Injectable()
export class ProductRepository {
  constructor(@InjectModel(Constants.ProductRef) private readonly productModel: Model<Product>) {
  }

  public create(product: Product): Promise<Product> {
    return this.productModel.create(product);
  }

  public findOne(condition: MongooseFilterQuery<Product>): Promise<Product> {
    return this.productModel.findOne(condition).exec();
  }

  public findByUser(filter: MongooseFilterQuery<Product>): Promise<Product[]> {
    return this.productModel.find(filter, {_id: false, barcode: true, name: true, allergens: true}).exec();
  }
}
