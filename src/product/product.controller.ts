import {Controller, Get, Param, Post, Query} from '@nestjs/common';
import {ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags} from '@nestjs/swagger';
import {IsEANPipe} from '../common/is-ean.pipe';
import {IsNotEmptyStringPipe} from '../common/is-not-empty-string.pipe';
import {Product} from './product';
import {ProductService} from './product.service';

@Controller('product')
@ApiTags('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {
  }

  @Post(':barcode')
  @ApiOperation({summary: 'Add the product'})
  @ApiCreatedResponse({description: 'Added product', type: Product})
  public add(@Param('barcode', IsEANPipe) barcode: string): Promise<Product> {
    return this.productService.add(barcode);
  }

  @Get()
  @ApiOperation({summary: 'Get products for group member'})
  @ApiOkResponse({description: 'All products', type: [Product]})
  public getByUser(@Query('username', IsNotEmptyStringPipe) username: string): Promise<Product[]> {
    return this.productService.getByUser(username);
  }
}
