import {HttpModule, Module} from '@nestjs/common';
import {ConfigModule} from '@nestjs/config';
import {MongooseModule} from '@nestjs/mongoose';
import {Constants} from '../common/constants';
import {UserModule} from '../user/user.module';
import {ProductSchema} from './product';
import {ProductController} from './product.controller';
import {ProductRepository} from './product.repository';
import {ProductService} from './product.service';

@Module({
  imports: [
    MongooseModule.forFeature([{name: Constants.ProductRef, schema: ProductSchema}]),
    HttpModule,
    ConfigModule,
    UserModule
  ],
  controllers: [ProductController],
  providers: [
    ProductService,
    ProductRepository
  ]
})
export class ProductModule {
}
