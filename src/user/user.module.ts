import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {Constants} from '../common/constants';
import {AllergenModule} from '../allergen/allergen.module';
import {UserSchema} from './user';
import {UserController} from './user.controller';
import {UserRepository} from './user.repository';
import {UserService} from './user.service';

@Module({
  imports: [
    MongooseModule.forFeature([{name: Constants.UserRef, schema: UserSchema}]),
    AllergenModule
  ],
  controllers: [UserController],
  providers: [
    UserService,
    UserRepository
  ],
  exports: [UserService]
})
export class UserModule {
}
