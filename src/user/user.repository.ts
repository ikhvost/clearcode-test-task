import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {Model, MongooseFilterQuery, MongooseUpdateQuery} from 'mongoose';
import {Constants} from '../common/constants';
import {User} from './user';

@Injectable()
export class UserRepository {
  constructor(@InjectModel(Constants.UserRef) private readonly userModel: Model<User>) {
  }

  public findAll(): Promise<User[]> {
    return this.userModel.find({}, {_id: false, name: true, allergens: true}).exec();
  }

  public findOne(filter: MongooseFilterQuery<User>): Promise<User> {
    return this.userModel.findOne(filter).exec();
  }

  public findOneAndUpdate(filter: MongooseFilterQuery<User>, update: MongooseUpdateQuery<User>): Promise<User> {
    return this.userModel.findOneAndUpdate(filter, update, {new: true, projection: {_id: false, name: true, allergens: true}}).exec();
  }

  public findOneAndDelete(filter: MongooseFilterQuery<User>) {
    return this.userModel.findOneAndDelete(filter, {projection: {_id: false, name: true, allergens: true}}).exec();
  }

  public create(user: User): Promise<User> {
    return this.userModel.create(user);
  }
}
