import {Body, Controller, Delete, Get, Patch, Post, Query} from '@nestjs/common';
import {ApiCreatedResponse, ApiOkResponse, ApiOperation, ApiTags} from '@nestjs/swagger';
import {IsNotEmptyStringPipe} from '../common/is-not-empty-string.pipe';
import {UpdateAllergenDto} from './dto/update-allergen.dto';
import {UserDto} from './dto/user.dto';
import {User} from './user';
import {UserService} from './user.service';

@Controller('user')
@ApiTags('user')
export class UserController {
  constructor(private readonly userService: UserService) {
  }

  @Get()
  @ApiOperation({summary: 'Get all group members'})
  @ApiOkResponse({description: 'All users', type: [User]})
  public getAll(): Promise<User[]> {
    return this.userService.getAll();
  }

  @Post()
  @ApiOperation({summary: 'Add group member'})
  @ApiCreatedResponse({description: 'Created user', type: User})
  public create(@Body() dto: UserDto): Promise<User> {
    return this.userService.create(dto);
  }

  @Patch()
  @ApiOperation({summary: 'Update group member data'})
  @ApiOkResponse({description: 'Updated user', type: User})
  public update(@Query('username', IsNotEmptyStringPipe) username: string,
                @Body() dto: UserDto): Promise<User> {
    return this.userService.updateData(username, dto);
  }

  @Delete()
  @ApiOperation({summary: 'Delete member'})
  @ApiOkResponse({description: 'Deleted user', type: User})
  public delete(@Query('username', IsNotEmptyStringPipe) username: string): Promise<User> {
    return this.userService.delete(username);
  }

  @Patch('allergen')
  @ApiOperation({summary: 'Assign allergens to group member'})
  @ApiOkResponse({description: 'Updated user', type: User})
  public updateAllergens(@Query('username', IsNotEmptyStringPipe) username: string,
                         @Body() dto: UpdateAllergenDto): Promise<User> {
    return this.userService.updateAllergens(username, dto);
  }
}
