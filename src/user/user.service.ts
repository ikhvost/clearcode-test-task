import {BadRequestException, Injectable, NotFoundException} from '@nestjs/common';
import {AllergenService} from '../allergen/allergen.service';
import {UpdateAllergenDto} from './dto/update-allergen.dto';
import {UserDto} from './dto/user.dto';
import {User} from './user';
import {UserRepository} from './user.repository';
import {uniq, difference} from 'lodash';
import {MongooseFilterQuery, MongooseUpdateQuery} from 'mongoose';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository,
              private readonly allergenService: AllergenService) {
  }

  public getAll(): Promise<User[]> {
    return this.userRepository.findAll();
  }

  public async getOneByName(name: string): Promise<User> {
    const user = await this.userRepository.findOne({name});
    if(!user) throw new NotFoundException('User not found');
    return user;
  }

  public async create(dto: UserDto): Promise<User> {
    await this.checkIfUserAlreadyExists(dto);
    return this.userRepository.create(dto as User);
  }

  public async delete(name: string): Promise<User> {
    const user = await this.userRepository.findOneAndDelete({name});
    if(!user) throw new NotFoundException('User not found');
    return user;
  }

  public async updateData(name: string, dto: UserDto): Promise<User> {
    await this.checkIfUserAlreadyExists(dto);
    return this.update({name}, {$set: {name: dto.name}});
  }

  public async updateAllergens(name: string, dto: UpdateAllergenDto): Promise<User> {
    const allergens = await this.allergenService.getAll();
    const wrongAllergens = difference(dto.allergens, allergens);
    if(wrongAllergens.length) throw new BadRequestException(`The following values are not an allergens: [${wrongAllergens}]`);

    return this.update({name}, {$set: {allergens: uniq(dto.allergens)}});
  }

  private async update(filter: MongooseFilterQuery<User>, update: MongooseUpdateQuery<User>): Promise<User> {
    const user = await this.userRepository.findOneAndUpdate(filter, update);
    if(!user) throw new NotFoundException('User not found');
    return user;
  }

  private async checkIfUserAlreadyExists(dto: UserDto): Promise<void> {
    const user = await this.userRepository.findOne({name: dto.name});
    if(user) throw new BadRequestException(`User with name '${user.name}' already exists`);
  }
}
