import {ApiProperty} from '@nestjs/swagger';
import {IsNotEmptyString} from '../../common/is-not-empty-string.validator';

export class UserDto {
  @ApiProperty()
  @IsNotEmptyString()
  name: string;
}
