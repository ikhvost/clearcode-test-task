import {ApiProperty} from '@nestjs/swagger';
import {ArrayNotEmpty, IsArray} from 'class-validator';
import {IsNotEmptyString} from '../../common/is-not-empty-string.validator';

export class UpdateAllergenDto {
  @ApiProperty()
  @IsArray()
  @ArrayNotEmpty()
  @IsNotEmptyString({each: true})
  allergens: string[];
}
