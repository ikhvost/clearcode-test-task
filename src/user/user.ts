import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {ApiProperty} from '@nestjs/swagger';
import {Document} from 'mongoose';
import {Constants} from '../common/constants';

@Schema({collection: Constants.UserRef})
export class User extends Document {
  @ApiProperty()
  @Prop({required: true, unique: true})
  name: string;

  @ApiProperty()
  @Prop()
  allergens: string[];
}

export const UserSchema = SchemaFactory.createForClass(User);
