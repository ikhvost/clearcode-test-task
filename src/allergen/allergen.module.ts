import {HttpModule, Module} from '@nestjs/common';
import {ConfigModule} from '@nestjs/config';
import {AllergenController} from './allergen.controller';
import {AllergenService} from './allergen.service';

@Module({
  imports: [
    HttpModule,
    ConfigModule
  ],
  controllers: [AllergenController],
  providers: [AllergenService],
  exports: [AllergenService]
})
export class AllergenModule {
}
