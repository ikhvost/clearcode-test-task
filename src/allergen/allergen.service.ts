import {HttpService, HttpStatus, Injectable, InternalServerErrorException} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';

@Injectable()
export class AllergenService {
  private readonly getAllergenEndpoint = '/data/taxonomies/allergens.json';

  constructor(private readonly http: HttpService,
              private readonly config: ConfigService) {
  }

  public async getAll(): Promise<string[]> {
    const result = await this.http.get(this.config.get('OPEN_FOOD_FACTS_API') + this.getAllergenEndpoint).toPromise();
    if(result?.status !== HttpStatus.OK) throw new InternalServerErrorException(`Can't get data from Open Food Facts API`);

    const allergens = Object.keys(result?.data).map((key) => key.split(':')[1]);
    return allergens;
  }
}
