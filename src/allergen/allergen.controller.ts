import {Controller, Get} from '@nestjs/common';
import {ApiOkResponse, ApiOperation, ApiTags} from '@nestjs/swagger';
import {AllergenService} from './allergen.service';

@Controller('allergen')
@ApiTags('allergen')
export class AllergenController {
  constructor(private readonly allergenService: AllergenService) {
  }

  @Get()
  @ApiOperation({summary: 'Get all allergens from Open Food Facts API'})
  @ApiOkResponse({description: 'List of allergens', type: [String]})
  public getAll(): Promise<string[]> {
    return this.allergenService.getAll();
  }
}
