import {Module} from '@nestjs/common';
import {ConfigModule, ConfigService} from '@nestjs/config';
import {MongooseModule} from '@nestjs/mongoose';
import {UserModule} from './user/user.module';
import {AllergenModule} from './allergen/allergen.module';
import {ProductModule} from './product/product.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env' + (!!process.env.NODE_ENV ? `.${process.env.NODE_ENV}` : ''),
      expandVariables: true
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => ({
        uri: config.get('DB_URI'),
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true,
        useUnifiedTopology: true
      }),
      inject: [ConfigService]
    }),
    UserModule,
    AllergenModule,
    ProductModule
  ]
})
export class AppModule {
}
