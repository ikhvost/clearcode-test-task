import {ValidationPipe} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';
import {HttpAdapterHost, NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {ExceptionFilter} from './common/exception.filter';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const {httpAdapter} = app.get(HttpAdapterHost);
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(new ExceptionFilter(httpAdapter));

  const config = app.get<ConfigService>(ConfigService);

  const options = new DocumentBuilder()
    .setTitle(config.get('SWAGGER_TITLE'))
    .setDescription(config.get('SWAGGER_DESCRIPTION'))
    .setVersion(config.get('SWAGGER_VERSION'))
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(config.get('APP_PORT'), config.get('APP_HOST'));
}
bootstrap();
