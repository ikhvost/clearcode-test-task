# Clearcode Recruitment Task

### Run with docker
1. Run `docker-compose up --build` (will be used `.env.prod` config).
2. API will be available on `http://APP_HOST:APP_PORT` (by default `http://localhost:3000`).

### API docs
1. On `API_ADDRESS/api` will be available Swagger UI.

### Run e2e tests
1. Run test database `docker-compose -f docker-compose-test.yaml up` (will be used `.env.test` config).
2. Run the e2e tests using the following command: `npm run test:e2e`.