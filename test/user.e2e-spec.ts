import {HttpStatus, INestApplication} from '@nestjs/common';
import {getConnectionToken} from '@nestjs/mongoose';
import * as st from 'supertest';
import {createTestDbUser, deleteAllTestDbUsers} from './utils/db.utils';
import {createTestApp, getBasicUser} from './utils/test.utils';
import {assignAllergensToTestUser, createTestUser, deleteTestUser, getTestUsers, updateTestUser} from './utils/user.test-requests';

describe('UserController (e2e)', () => {
  let app: INestApplication;
  let api: st.SuperTest<st.Test>;
  let dbConnection;

  beforeAll(async () => {
    app = await createTestApp();
    await app.init();
    api = st(app.getHttpServer());
    dbConnection = app.get(getConnectionToken());
  });

  afterAll(() => app.close());

  describe('/user (GET)', () => {
    afterAll(() => deleteAllTestDbUsers(dbConnection));

    it('Should return empty array when users not found', async () => {
      const result = await getTestUsers(api);
      expect(result).toEqual([]);
    });

    it('Should return array with the user', async () => {
      const user = getBasicUser('User', []);
      await createTestDbUser(dbConnection, user);

      const result = await getTestUsers(api);

      expect(result.length).toEqual(1);
      expect(result[0].name).toEqual(user.name);
    });
  });

  describe('/user (POST)', () => {
    afterEach(() => deleteAllTestDbUsers(dbConnection));

    it('Should throw BadRequest when name has not provided', () =>
      createTestUser(api, {}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when name is not a string', () =>
      createTestUser(api, {name: 1111}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when name is an empty string', () =>
      createTestUser(api, {name: ''}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when name is a blank space string', () =>
      createTestUser(api, {name: '    '}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when the user with the same name already exists', async () => {
      const user = getBasicUser('User', []);
      await createTestDbUser(dbConnection, user);
      await createTestUser(api, {name: user.name}, HttpStatus.BAD_REQUEST);
    });

    it('Should return user when successfully created', async () => {
      const user = getBasicUser('User', []);
      const result = await createTestUser(api, user);
      expect(result.name).toEqual(user.name);
    });
  });

  describe('/user (PATCH)', () => {
    const user = getBasicUser('User', []);

    beforeAll(() => createTestDbUser(dbConnection, user));

    afterAll(() => deleteAllTestDbUsers(dbConnection));

    it('Should throw BadRequest when username has not provided', () =>
      updateTestUser(api, undefined, {name: user.name}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when username is an empty string', () =>
      updateTestUser(api, '', {name: user.name}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when username is a blank space string', () =>
      updateTestUser(api, '      ', {name: user.name}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when new name has not provided', () =>
      updateTestUser(api, user.name, {}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when new name is not a string', () =>
      updateTestUser(api, user.name, {name: 1111}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when new name is an empty string', () =>
      updateTestUser(api, user.name, {name: ''}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when new name is a blank space string', () =>
      updateTestUser(api, user.name, {name: '    '}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when user with the same name already exists', () =>
      updateTestUser(api, user.name, {name: user.name}, HttpStatus.BAD_REQUEST)
    );

    it('Should throw NotFound when the user not found', async () => {
      const newUser = getBasicUser('NewUser', []);
      await updateTestUser(api, newUser.name, {name: newUser.name}, HttpStatus.NOT_FOUND);
    })

    it('Should return the user when successfully updated', async () => {
      const newUser = getBasicUser('NewUser', []);
      const result = await updateTestUser(api, user.name, {name: newUser.name});
      expect(result.name).not.toEqual(user.name);
      expect(result.name).toEqual(newUser.name);
    });
  });

  describe('/user (DELETE)', () => {
    const user = getBasicUser('User', []);

    beforeAll(() => createTestDbUser(dbConnection, user));

    afterAll(() => deleteAllTestDbUsers(dbConnection));

    it('Should throw BadRequest when username has not provided', () =>
      deleteTestUser(api, undefined, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when username is an empty string', () =>
      deleteTestUser(api, '', HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when username is a blank space string', () =>
      deleteTestUser(api, '      ', HttpStatus.BAD_REQUEST)
    );

    it('Should throw NotFound when the user not found', () =>
      deleteTestUser(api, 'NewUser', HttpStatus.NOT_FOUND)
    );

    it('Should return empty array when the user successfully deleted', async () => {
      await deleteTestUser(api, user.name);
      const result = await getTestUsers(api);
      expect(result).toEqual([]);
    });
  });

  describe('/user/allergen', () => {
    const user = getBasicUser('User', []);

    beforeAll(() => createTestDbUser(dbConnection, user));

    afterAll(() => deleteAllTestDbUsers(dbConnection));

    it('Should throw BadRequest when username has not provided', () =>
      assignAllergensToTestUser(api, undefined, ['milk'], HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when username is an empty string', () =>
      assignAllergensToTestUser(api, '', ['milk'], HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when username is a blank space string', () =>
    assignAllergensToTestUser(api, '       ', ['milk'], HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when allergens is not an array', () =>
      assignAllergensToTestUser(api, user.name, 'milk', HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when allergens is an empty array', () =>
      assignAllergensToTestUser(api, user.name, [], HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when allergens item is not a string', () =>
      assignAllergensToTestUser(api, user.name, [111], HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when allergens item is an empty string', () =>
      assignAllergensToTestUser(api, user.name, [''], HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when allergens item is a blank space string', () =>
      assignAllergensToTestUser(api, user.name, ['     '], HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when wrong allergen has provided', () =>
      assignAllergensToTestUser(api, user.name, ['qwerty'], HttpStatus.BAD_REQUEST)
    );

    it('Should return user when successfully updated', async () => {
      const allergens = ['milk'];
      const result = await assignAllergensToTestUser(api, user.name, allergens);
      expect(result.name).toEqual(user.name);
      expect(result.allergens[0]).toEqual(allergens[0]);
    });

    it('Should return one allergen if the duplicate has been provided', async () => {
      const allergens = ['milk', 'milk'];
      const result = await assignAllergensToTestUser(api, user.name, allergens);
      expect(result.allergens.length).toEqual(1);
    });
  });
});