import {HttpStatus, Logger} from '@nestjs/common';
import {SuperTest, Test} from 'supertest';

const context = 'Allergen test requests';

export async function getTestAllegrens(api: SuperTest<Test>, expectedCode = HttpStatus.OK) {
  const response = await api
    .get('/allergen')
    .expect(expectedCode);
  Logger.log(`Get test allergens. Status '${response.status}'`, context);
  return response.body;
}
