import {HttpStatus, Logger} from '@nestjs/common';
import {SuperTest, Test} from 'supertest';

const context = 'Product test requests';

export async function getProductsByUser(api: SuperTest<Test>, username, expectedCode = HttpStatus.OK) {
  const response = await api
    .get('/product')
    .query({username})
    .expect(expectedCode);
  Logger.log(`Get test products by user. Status '${response.status}'`, context);
  return response.body;
}

export async function addTestProduct(api: SuperTest<Test>, barcode, expectedCode = HttpStatus.CREATED) {
  const response = await api
    .post(`/product/${barcode}`)
    .expect(expectedCode);
  Logger.log(`Create test product '${barcode}'. Status '${response.status}'`, context);
  return response.body;
}
