import {HttpStatus, Logger} from '@nestjs/common';
import {SuperTest, Test} from 'supertest';

const context = 'User test requests';

export async function getTestUsers(api: SuperTest<Test>, expectedCode = HttpStatus.OK) {
  const response = await api
    .get('/user')
    .expect(expectedCode);
  Logger.log(`Get test users. Status '${response.status}'`, context);
  return response.body;
}

export async function createTestUser(api: SuperTest<Test>, user, expectedCode = HttpStatus.CREATED) {
  const response = await api
    .post('/user')
    .send(user)
    .expect(expectedCode);
  Logger.log(`Create test user '${user.name}'. Status '${response.status}'`, context);
  return response.body;
}

export async function updateTestUser(api: SuperTest<Test>, username, user, expectedCode = HttpStatus.OK) {
  const response = await api
    .patch(`/user`)
    .query({username})
    .send(user)
    .expect(expectedCode);
  Logger.log(`Update user with name '${username}'. Status '${response.status}'`, context);
  return response.body;
}

export async function deleteTestUser(api: SuperTest<Test>, username, expectedCode = HttpStatus.OK) {
  const response = await api
    .delete(`/user`)
    .query({username})
    .expect(expectedCode);
  Logger.log(`Delete user with name '${username}'. Status '${response.status}'`, context);
  return response.body;
}

export async function assignAllergensToTestUser(api: SuperTest<Test>, username, allergens, expectedCode = HttpStatus.OK) {
  const response = await api
    .patch(`/user/allergen`)
    .query({username})
    .send({allergens})
    .expect(expectedCode);
  Logger.log(`Assign allergens to test user with id '${username}'. Status '${response.status}'`, context);
  return response.body;
}
