import {Logger} from '@nestjs/common';
import {Constants} from '../../src/common/constants';
import {Product} from '../../src/product/product';
import {User} from '../../src/user/user';

const context = 'Test DB methods';

export async function createTestDbUser(connection, user: User): Promise<void> {
  try {
    const userModel = connection.model(Constants.UserRef);
    await userModel.create(user);
    Logger.log(`Created test user '${user.name}'`, context);
  } catch(error) {
    Logger.error(`Can't create test user '${user.name}'. ${error.message}`, null, context);
  }
}

export async function deleteAllTestDbUsers(connection): Promise<void> {
  try {
    const userModel = connection.model(Constants.UserRef);
    await userModel.deleteMany({});
    Logger.log(`All users have deleted`, context);
  } catch(error) {
    Logger.error(`Can't delete all users. ${error.message}`, null, context);
  }
}

export async function createTestDbProduct(connection, product: Product): Promise<void> {
  try {
    const productModel = connection.model(Constants.ProductRef);
    await productModel.create(product);
    Logger.log(`Created test product '${product.name}'`, context);
  } catch(error) {
    Logger.error(`Can't create test product '${product.name}'. ${error.message}`, null, context);
  }
}

export async function deleteAllTestDbProducts(connection): Promise<void> {
  try {
    const productModel = connection.model(Constants.ProductRef);
    await productModel.deleteMany({});
    Logger.log(`All products have deleted`, context);
  } catch(error) {
    Logger.error(`Can't delete all products. ${error.message}`, null, context);
  }
}
