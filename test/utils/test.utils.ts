import {User} from '../../src/user/user';
import {Test} from '@nestjs/testing';
import {AppModule} from '../../src/app.module';
import {INestApplication, ValidationPipe} from '@nestjs/common';
import {Product} from '../../src/product/product';

export function getBasicUser(name: string, allergens: string[]): User {
  return {name, allergens} as User;
}

export function getBasicProduct(allergens?: string[]): Product {
  return {
    barcode: '8712100325953',
    name: 'Moutarde de Dijon Fine et Forte',
    allergens: allergens?.length ? allergens : [
      'mustard',
      'sulphur-dioxide-and-sulphites'
    ]
  } as Product;
}

export async function createTestApp(): Promise<INestApplication> {
  const appModule = await Test.createTestingModule({
    imports: [AppModule]
  }).compile();

  const app = appModule.createNestApplication();
  app.useGlobalPipes(new ValidationPipe());

  return app;
}