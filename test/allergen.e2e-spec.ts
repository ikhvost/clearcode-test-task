import {INestApplication} from '@nestjs/common';
import * as st from 'supertest';
import {getTestAllegrens} from './utils/allergen.test-request';
import {createTestApp} from './utils/test.utils';

describe('AllergenController (e2e)', () => {
  let app: INestApplication;
  let api: st.SuperTest<st.Test>;

  beforeAll(async () => {
    app = await createTestApp();
    await app.init();
    api = st(app.getHttpServer());
  });

  afterAll(() => app.close());

  describe('/allergen (GET)', () => {
    it('Should return all allergens from OPEN FOOD FACTS API', async () => {
      const result = await getTestAllegrens(api);
      expect(result.length).toBeGreaterThan(0);
      expect(typeof result[0]).toEqual('string');
    });
  });
});
