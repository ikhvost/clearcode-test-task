import {HttpStatus, INestApplication} from '@nestjs/common';
import {getConnectionToken} from '@nestjs/mongoose';
import * as st from 'supertest';
import {isEqual} from 'lodash';
import {createTestDbProduct, createTestDbUser, deleteAllTestDbProducts, deleteAllTestDbUsers} from './utils/db.utils';
import {addTestProduct, getProductsByUser} from './utils/product.test-requests';
import {createTestApp, getBasicProduct, getBasicUser} from './utils/test.utils';

describe('ProductController (e2e)', () => {
  let app: INestApplication;
  let api: st.SuperTest<st.Test>;
  let dbConnection;

  beforeAll(async () => {
    app = await createTestApp();
    await app.init();
    api = st(app.getHttpServer());
    dbConnection = app.get(getConnectionToken());
  });

  afterAll(() => app.close());

  describe('/product/:barcode (POST)', () => {
    it('Should throw BadRequest when barcode has not provided', () => 
      addTestProduct(api, undefined, HttpStatus.BAD_REQUEST)
    );

    it('Should throw NotFound when barcode is an empty string', () => 
      addTestProduct(api, '', HttpStatus.NOT_FOUND)
    );

    it('Should throw BadRequest when barcode has not an EAN format', () => 
      addTestProduct(api, '111111', HttpStatus.BAD_REQUEST)
    );

    it('Should throw NotFound when the product not found', () => 
      addTestProduct(api, '5906441350764', HttpStatus.NOT_FOUND)
    );

    it('Should throw BadRequest when the product with same barcode already exists', async () => {
      const product = getBasicProduct();
      await createTestDbProduct(dbConnection, product);
      await addTestProduct(api, product.barcode, HttpStatus.BAD_REQUEST);
      await deleteAllTestDbProducts(dbConnection);
    });

    it('Should return product when successfully added', async () => {
      const product = getBasicProduct();
      const result = await addTestProduct(api, product.barcode);

      expect(result.barcode).toEqual(product.barcode);
      expect(result.name).toEqual(product.name);
      expect(isEqual(product.allergens, result.allergens)).toBeTruthy();

      await deleteAllTestDbProducts(dbConnection);
    });
  });

  describe('/product (GET)', () => {
    const product = getBasicProduct();

    beforeAll(() => createTestDbProduct(dbConnection, product));

    afterAll(() => deleteAllTestDbProducts(dbConnection));

    it('Should throw BadRequest when username has not provided', () =>
      getProductsByUser(api, undefined, HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when username is an empty string', () =>
      getProductsByUser(api, '', HttpStatus.BAD_REQUEST)
    );

    it('Should throw BadRequest when username is a blank space string', () =>
      getProductsByUser(api, '      ', HttpStatus.BAD_REQUEST)
    );

    it('Should throw NotFound when the user not found', () =>
      getProductsByUser(api, 'NewUser', HttpStatus.NOT_FOUND)
    );

    it('Should return the empty array when the user has an allergen as in the product.', async () => {
      const user = getBasicUser('User', product.allergens);
      await createTestDbUser(dbConnection, user);

      const result = await getProductsByUser(api, user.name);
      expect(result.length).toEqual(0);

      await deleteAllTestDbUsers(dbConnection);
    });

    it('Should return the product when the user has not an allergen as in the product.', async () => {
      const user = getBasicUser('User', []);
      await createTestDbUser(dbConnection, user);

      const result = await getProductsByUser(api, user.name);
      expect(result.length).toEqual(1);
      expect(result[0].name).toEqual(product.name);

      await deleteAllTestDbUsers(dbConnection);
    });
  });
});
